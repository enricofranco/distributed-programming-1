<?php
$host = "localhost";
$userDB = "root";
$passwordDB = "root";
$database = "s251102";
$SHUTTLE_CAPACITY = 4;
$TIMEOUT = 120; // No action for more than 2 minutes will cause logout

$SESSION_PREFIX = "s251102";

function connect() {
	$connection = mysqli_connect($GLOBALS["host"], $GLOBALS["userDB"], $GLOBALS["passwordDB"], $GLOBALS["database"])
		or die("Connect error (" . mysqli_connect_errno() . ")" . mysqli_connect_error());

	return $connection;
}

function close($connection) {
	mysqli_close($connection)
		or die("Close error (" . mysqli_connect_errno() . ")" . mysqli_connect_error());
}

function query($connection, $query) {
	$result = mysqli_query($connection, $query)
		or die("Query error (" . mysqli_connect_errno() . ")" . mysqli_connect_error());
	return $result;
}

function hashPassword($password) {
	return md5($password);
}

/**
 * This function returns an associative array with fields numberOfPeople, numberOfUsers, array users, array people.
 * The array stores the number of people carried on or travelling through a city, together with user info,
 * indicated in the array index.
 * Any concurrent booking is stopped on this function.
 * @param $connection
 * @return $bookings
 */
function retrievePeople($connection) {
	$result = query($connection, "SELECT `city` FROM `shuttle` ORDER BY `city` FOR UPDATE");
	$rows = mysqli_num_rows($result);
	$bookings = array();

	for($i = 0; $i < $rows; $i++) {
		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
		$index = $row['city'];

		// Store the number of people carried on or travelling through that city
		$bookings[$index]['numberOfPeople'] = 0;
		$bookings[$index]['numberOfUsers'] = 0;
		$bookings[$index]['users'] = array();
		$bookings[$index]['people'] = array();
	}
	mysqli_free_result($result);

	// Lock the entire table. In this way, any concurrent booking will wait for the completion of previous ones.
	// Any concurrent booking will be stopped here (if included in a transaction).
	$result = query($connection, "SELECT * FROM `booking` ORDER BY `departure`, `arrival` FOR UPDATE");
	$rows = mysqli_num_rows($result);
	for($i = 0; $i < $rows; $i++) {
		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
		$username = $row['username'];
		$departure = $row['departure'];
		$arrival = $row['arrival'];
		$numPeople = $row['people'];

		// Update cities
		foreach($bookings as $key => $value) {
			// If the city is in the trip booked by the user
			if(strcmp($key, $departure) >= 0 &&  strcmp($key, $arrival) < 0) {
				$bookings[$key]['numberOfPeople'] += $numPeople; // Add people to the city
				$userIndex = $bookings[$key]['numberOfUsers']; // Index in the 'users' array
				$bookings[$key]['users'][$userIndex] = $username; // Add the user
				$bookings[$key]['people'][$userIndex] = $numPeople; // Add the people for the user
				$bookings[$key]['numberOfUsers']++; // Update number of users in the city
				}
		}
	}

	mysqli_free_result($result);

	foreach($bookings as $city => $booking) {
		sort($bookings[$city]['users']);
	}

	return $bookings;
}

function sanitizeString($var) {
	$var = strip_tags($var);
	$var = htmlentities($var);
	$var = stripslashes($var);
	return $var;
}

function checkSessionValidity() {
	$SESSION_PREFIX = $GLOBALS['SESSION_PREFIX'];
	if(isset($_SESSION[$SESSION_PREFIX . 'authorized']) && isset($_SESSION[$SESSION_PREFIX . 'username'])) {
		if(time() - $_SESSION[$SESSION_PREFIX . 'time'] > $GLOBALS['TIMEOUT']) { // Timeout expired
			$_SESSION = array();
			// If it's desired to kill the session, also delete the session cookie.
			// Note: This will destroy the session, and not just the session data!
			if (ini_get("session.use_cookies")) {
				$params = session_get_cookie_params();
				setcookie(session_name(), '', time() - 3600*24,
				$params["path"], $params["domain"],
				$params["secure"], $params["httponly"]
				);
			}
			session_destroy(); // Destroy session
			return false;
		} else {
			$_SESSION[$SESSION_PREFIX . 'time'] = time();
			return true;
		}
	}

	return false;
}

function redirectHTTPS() {
	if($_SERVER['HTTPS'] != "on") {
		$redirect = "https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		header("location: $redirect");
	}
}

function bookedUser($username) {
	$connection = connect();
	$statement = mysqli_stmt_init($connection);
	mysqli_stmt_prepare($statement, "SELECT * FROM `booking` WHERE `username` = ?");
	mysqli_stmt_bind_param($statement, 's', $username);
	mysqli_stmt_execute($statement);
	mysqli_stmt_store_result($statement);
	$counter = mysqli_stmt_num_rows($statement);
	mysqli_stmt_free_result($statement);
	mysqli_stmt_close($statement);
	close($connection);

	return ($counter == 1);
}

function checkAvailability($bookings, $cities, $indexStartingCity, $indexEndingCity, $passengers) {
	$bookingOk = true;
	if($indexStartingCity < 0)
		return true;

	for($i = $indexStartingCity; $i <= $indexEndingCity; $i++) {
		$city = $cities[$i]; // Needed to access bookings array
		$numberOfPeople = $bookings[$city]['numberOfPeople'];
		$available = $GLOBALS["SHUTTLE_CAPACITY"] - $numberOfPeople;
		if($available < $passengers) {
			$bookingOk = false;
			break;
		}
	}
	return $bookingOk;
}

function checkCookies() {
	if(isset($_GET['cookiecheck'])) {
		if(isset($_COOKIE['testcookie'])) {
			// Cookies enabled, load the page without the "bad" part
			header("Location: " . str_replace("?cookiecheck=1", "", $_SERVER['PHP_SELF']));
		} else {
			// Cookies disabled
			header("Location: nocookies.php");
			exit();
		}
	} else {
		if(! isset($_COOKIE['testcookie'])) {
			// Here only when loading page the first time, try to set a cookie
			setcookie('testcookie', "testvalue");
			header("Location: " . $_SERVER['PHP_SELF'] . "?cookiecheck=1");
			exit();
		}
	}
}
?>