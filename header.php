<?php
	if(count($_COOKIE) < 1) {
		// Cookies not enabled
		// Show an error message
		exit();
	}
?>

<div class="container-fluid">
	<div class="page-header">
    	<h1>
    		Shuttle booking <small>Here you can book a place for your trip!</small>
    	</h1>
	</div> <!-- End of page header -->
	<div class="row">
    	<noscript>
    		<div class="col-md-12 bg-danger">
        		<h4 class="text-center text-warning">WARNING: Your browser does not support or has disabled javascript. This site may not work properly.</h4>
    		</div>
    	</noscript>
    	<div class="col-md-3">
        	<ul class="nav nav-pills nav-stacked">

<?php
    if(! checkSessionValidity()) {
        // User not authenticated
?>
        		<li id="home">
        			<a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a>
        		</li>
        		<li id="signup">
        			<a href="registration.php"><span class="glyphicon glyphicon-pencil"></span> Sign up</a>
        		</li>
        		<li id="login">
        			<a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a>
        		</li>
<?php
    } else {
        // Authenticated user
?>
        		<li class="active">
        			<a><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION[$SESSION_PREFIX . 'username']; ?></a>
        		</li>
<?php
        if(! bookedUser($_SESSION[$SESSION_PREFIX . 'username'])) {
            // User has not booked yet
?>
				<li id="home">
					<a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a>
				</li>
				<li id="book">
        			<a href="booking.php"><span class="glyphicon glyphicon-pushpin"></span> Book</a>
        		</li>
<?php
        } else {
            // Userhas already booked
?>
        		<li id="mybooking">
        			<a href="mybooking.php"><span class="glyphicon glyphicon-pushpin"></span> My booking</a>
        		</li>
<?php
    }
?>
        		<li id="logout">
        			<a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
        		</li>
<?php
    }
?>
        	</ul>
        </div> <!-- End of menu -->
