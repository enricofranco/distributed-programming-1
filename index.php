<?php
	include('functions.php');
	redirectHTTPS();
	checkCookies();
	session_start();

	$valid = checkSessionValidity();
	if($valid && bookedUser($_SESSION[$SESSION_PREFIX . 'username'])) // If the user is logged, show his booking
		header("location: mybooking.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Enrico Franco">
	<title>Shuttle</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
</head>
<body>
<?php
	include('header.php');
?>
<div class="col-md-9">
<?php
	if(isset($_SESSION[$SESSION_PREFIX . 'message'])) {
		echo $_SESSION[$SESSION_PREFIX . 'message'];
		unset($_SESSION[$SESSION_PREFIX . 'message']);
	}

	$connection = connect();
	$bookings = retrievePeople($connection);
	close($connection);

	// Detect cities
	$cities = array();
	$numberOfCities = 0;

	foreach($bookings as $city => $values) {
		$cities[$numberOfCities++] = $city;
	}
?>
	<h2><span class="glyphicons glyphicon glyphicon-globe"></span> Our destinations</h2>
<?php
	echo "<p>Our shuttle can carry up to " . $GLOBALS["SHUTTLE_CAPACITY"] . " people!"; // Sshuttle capacity
?>
	<div class="table-responsive">
		<table class="table">
			<tbody>
				<tr>
					<th colspan="3">Segment</th>
					<th></th>
					<th>Booked users</th>
				</tr>
<?php
	if($numberOfCities > 0) {
		for($i = 0; $i < $numberOfCities - 1; $i++) {
			$city = $cities[$i]; // Needed to access bookings array

			echo "<tr><td>" . $cities[$i] . "<td><span class=\"glyphicon glyphicon-arrow-right\"></span><td>" . $cities[$i+1]; // Segment
			$numberOfPeople = $bookings[$city]['numberOfPeople'];
			if($numberOfPeople == 0) {
				$string = "<td>Nobody has already booked this trip!";
			} else if($numberOfPeople == 1) {
				$string = "<td>1 person has already booked this trip!";
			} else {
				$string = "<td>" . $numberOfPeople . " people have already booked this trip!";
			}

			$available = $GLOBALS["SHUTTLE_CAPACITY"] - $numberOfPeople;

			$string .= " " . ($GLOBALS["SHUTTLE_CAPACITY"] - $numberOfPeople) . " place";
			$string .= ($available != 1 ? "s are" : " is") . " still available.";

			echo $string; // People and availability

			// If the user is correctly logged in, show the booked users
			if($valid) {
				$string = "<td>Booked users: ";
				if($numberOfPeople > 0) {
					foreach($bookings[$city]['users'] as $index => $user) {
						$people = $bookings[$city]['people'][$index];
						$string .= $user . " (" . $people . ($people > 1 ? " places" : " place") . "), ";
					}
					$string = substr($string, 0, -2);
				} else {
					$string .= "None";
				}
			} else {
				$string = "<td>Login to view booked users.";
			}

			echo $string; // Booked users
		}
	} else {
		echo "<tr><td colspan=\"5\">No destinations are available at the moment. Make a booking and add them!</td></tr>";
	}
?>
			</tbody>
		</table>
	</div> <!-- End of table -->
</div>  <!-- End of content -->
<?php
	include('footer.php');
?>
<script type="text/javascript"><!--
	document.getElementById("home").className = "active";
//--></script>
</body>
</html>