<?php
	include('functions.php');
	redirectHTTPS();
	checkCookies();
	session_start();

	if(checkSessionValidity()) {
		header("location: index.php");
	}

	if(isset($_REQUEST['submit'])) {
		$connection = connect();
		$statement = mysqli_stmt_init($connection);
		mysqli_stmt_prepare($statement, "SELECT * FROM `user` WHERE `username` = ? AND `password` = ?");
		$user = sanitizeString($_REQUEST['email']);
		$user = mysqli_escape_string($connection, $user);
		$password = hashPassword($_REQUEST['password']);
		mysqli_stmt_bind_param($statement, 'ss', $user, $password);
		mysqli_stmt_execute($statement);
		mysqli_stmt_store_result($statement);
		$numberOfRows = mysqli_stmt_num_rows($statement); // Count how many matches
		mysqli_stmt_free_result($statement);
		mysqli_stmt_close($statement);
		close($connection);

		if($numberOfRows == 1) { // Login authorized
			// Save data in session
			$_SESSION[$SESSION_PREFIX . 'username'] = $user;
			$_SESSION[$SESSION_PREFIX . 'authorized'] = true;
			$_SESSION[$SESSION_PREFIX . 'time'] = time();

			$_SESSION[$SESSION_PREFIX . 'message'] = "<div class=\"container-fluid bg-success text-info\"><h4>You are now logged in!</h4></div>";
			// Redirect to user home page
			header("location: index.php");
			exit();
		} else {
			// Unauthorized, prepare a string
			$unauthorized = "<div class=\"container-fluid bg-danger text-warning\"><h4>You are not allowed.</h4></div>";
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Enrico Franco">
	<title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
</head>
<body>
<?php
	include('header.php');
?>
<div class="col-md-9">
<?php
	if(isset($unauthorized))
		echo $unauthorized;
?>
	<h2><span class="glyphicons glyphicon glyphicon-log-in"></span> Login</h2>
	<form class="form-horizontal" method="post" action="login.php">
		<div class="form-group">
			<label class="control-label col-sm-2" for="email">Email:</label>
			<div class="col-sm-10">
			  <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required="required">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="password">Enter password:</label>
			<div class="col-sm-10">
			  <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" required="required">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="reset" class="btn btn-danger">Cancel</button>
				<button type="submit" class="btn btn-success" name="submit">Submit</button>
			</div>
		</div>
	</form>
</div>
<?php
	include('footer.php');
?>
<script type="text/javascript"><!--
	document.getElementById("login").className = "active";
//--></script>
</body>
</html>