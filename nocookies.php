<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Enrico Franco">
	<title>No cookies</title>
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
</head>
<body class="bg-danger">
	<div class="container-fluid bg-danger">
		<h1 class="text-warning text-center">Cookies must be enabled to browse this website. Check your browser configuration.</h1>
		<a href="index.php"><h3 class="text-center">Click here to resume navigation!</h3></a>
	</div>
</body>
</html>