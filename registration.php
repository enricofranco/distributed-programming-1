<?php
	include('functions.php');
	redirectHTTPS();
	checkCookies();
	session_start();

	if(isset($_REQUEST['submit'])) {
		if(! isset($_REQUEST['email'])
		|| ! isset($_REQUEST['password'])
		|| ! isset($_REQUEST['password_confirm'])) {
			$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>Please, complete all fields.</h4></div>";
			goto displayPage;
		}

		$user = sanitizeString($_REQUEST['email']);
		if(! filter_var($user, FILTER_VALIDATE_EMAIL)) {
			$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>Please, insert a valid mail.</h4></div>";
			goto displayPage;
		}

		if($_REQUEST['password'] != $_REQUEST['password_confirm']) {
			$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>Passwords are different.</h4></div>";
			goto displayPage;
		}

		$password = hashPassword($_REQUEST['password']);
		if(preg_match("/(?=.*[a-z])(?=.*[A-Z0-9]).{0,}/m", $_REQUEST['password']) == 0) {
			$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>Use a valid and robust password.</h4></div>";
			goto displayPage;
		}

		$connection = connect();
		try {
			mysqli_autocommit($connection, false);
			mysqli_begin_transaction($connection);

			$statement = mysqli_stmt_init($connection);
			mysqli_stmt_prepare($statement, "SELECT * FROM `user` WHERE `username` = ?"); // Be sure that user does not exist yet
			$user = mysqli_escape_string($connection, $user);
			mysqli_stmt_bind_param($statement, 's', $user);
			if(! mysqli_stmt_execute($statement))
				throw new Exception();
			mysqli_stmt_store_result($statement);
			$numberOfRows = mysqli_stmt_num_rows($statement); // Count how many matches
			mysqli_stmt_free_result($statement);
			mysqli_stmt_close($statement);

			if($numberOfRows == 0) {
				// No matches
				$statement = mysqli_stmt_init($connection);
				mysqli_stmt_prepare($statement, "INSERT INTO `user` (`username`, `password`) VALUES (?, ?)");
				mysqli_stmt_bind_param($statement, 'ss', $user, $password);
				if(! mysqli_stmt_execute($statement))
				   throw new Exception();
				mysqli_stmt_close($statement);
				mysqli_commit($connection);
				close($connection);

				// Save data in session
				$_SESSION[$SESSION_PREFIX . 'username'] = $user;
				$_SESSION[$SESSION_PREFIX . 'authorized'] = true;
				$_SESSION[$SESSION_PREFIX . 'time'] = time();

				$_SESSION[$SESSION_PREFIX . 'message'] = "<div class=\"container-fluid bg-success text-info\"><h4>You are now registered and automatically logged in!</h4></div>";
				// Redirect to a success page
				header("location: index.php");
				exit();
			} else {
				throw new Exception();
			}
		} catch(Exception $e) {
			mysqli_rollback($connection);
			$error = "<div class=\"container-fluid bg-danger text-warning\"><h4>User already exists.</h4></div>";
		}

		mysqli_autocommit($connection, true);
		close($connection);
	}

	displayPage: // Label
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="author" content="Enrico Franco">
	<title>Registration</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
</head>
<body>
<?php
	include('header.php');
?>
<div class="col-md-9">
<?php
	if(isset($error))
		echo $error;
?>
	<h2><span class="glyphicons glyphicon glyphicon-pencil"></span> Registration form</h2>
	<form class="form-horizontal" action="registration.php" method="post">
		<div class="form-group">
			<label class="control-label col-sm-2" for="email">Email:</label>
			<div class="col-sm-10">
			  <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required="required">
			</div>
		</div>
		<div class="form-group" id="div_psw">
			<label class="control-label col-sm-2" for="password">Enter password:</label>
			<div class="col-sm-10">
			  <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" pattern="(?=.*[a-z])(?=.*[A-Z0-9]).{0,}" title="Pasword must contain at least one lowercase alphabetic character, and at least another character which must be either an uppercase alphabetic character or a digit" required="required" onkeyup="checkpwd()"> <!-- Lower and upper or digits, length greater than zero -->
			</div>
		</div>
		<div class="form-group" id="div_psw-repeat">
			<label class="control-label col-sm-2" for="password_confirm">Confirm password:</label>
			<div class="col-sm-10">
			  <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm password" required="required">
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="reset" class="btn btn-danger">Cancel</button>
				<button type="submit" class="btn btn-success" id="submit" name="submit">Submit</button>
			</div>
		</div>
	</form>
</div>
<?php
	include('footer.php');
?>
<script type="text/javascript"><!--
var password = document.getElementById("password");
var confirm_password = document.getElementById("password_confirm");
var div_psw = document.getElementById("div_psw");
var div_psw_repeat = document.getElementById("div_psw-repeat");

function validatePassword() {
	if(password.value != confirm_password.value) {
		confirm_password.setCustomValidity("Passwords do not match");
		div_psw_repeat.setAttribute("class", "form-group has-error has-feedback");
	} else {
		confirm_password.setCustomValidity("");
		div_psw_repeat.setAttribute("class", "form-group has-success has-feedback");
	}
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

// When the user starts to type something inside the password field
password.onkeyup = function() {
	// Validate lowercase letters
	var lowerCaseLetters = /[a-z]/g;
	// Validate capital letters
	var upperCaseLetters = /[A-Z0-9]/g;

	// Manage color
	if(password.value.match(lowerCaseLetters) && password.value.match(upperCaseLetters)) {
		div_psw.setAttribute("class", "form-group has-success has-feedback");
	} else {
		div_psw.setAttribute("class", "form-group has-error has-feedback");
	}

	validatePassword();
}

confirm_password.onchange = password.onkeyup;

//--></script>
<script type="text/javascript"><!--
	document.getElementById("signup").className = "active";
//--></script>
</body>
</html>